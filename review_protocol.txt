# Search Protocol - Group 26

Search string

~~~~~ {. search string}
(Potential OR pros) AND (limit OR cons) AND ("deep learning" OR "machine learning" OR learning) AND (robot OR robots OR robotic OR robotics)
~~~~~

Number of papers: 85

Inclusion criteria:

1. Publications released on or before 1st quarter of 2021, i.e., on or before 31st March, 2021.
2. Publications in journals and conferences only.
3. Publications that partially or entirely answers the research question.

Exclusion criteria:

1. Publications that are not edited to their final print version.
2. Publications that are not in English.


